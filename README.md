# WebRadios

Play web-radios on VLC using a simple sh-script. You can then use a hedless (SSH) access to your computer to launch it. Usefull for RaspberryPi for instance, used to control the sound.

## Installation

Clone the repository in a folder.

## Usage

Before the first use, you have to make `radio` file executable : `sudo chmod 755 radio` in a terminal.

Launch `./radio` from a terminal, and choose the web-radio you want to play: Type the corresponding number into the terminal, it should start playing the radio. To stop the radio, `kill` the `vlc` jobs. You need the `PID` of `vlc` to `kill` it. The list of all jobs can be found using the `ps` command. Then call `kill [pid-number]` with `[pid-number]` replaced by the `PID` number found using `ps`.

If you want to add a web-radio, edit a new text file, whose name will be the name displayed by the script. The content of the text file is the adress of the flux of the radio. See exemples given for more details.

## License
MIT License
